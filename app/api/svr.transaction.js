/**
 * @author Akshay Misal
 * @version 1.0.0
 * @since 03-Aug-2018
 */
var express = require('express');
var router = express.Router();
var Q = require('q');
var jwt = require('jsonwebtoken');
var config = require('../../config/config.json');
var mongo = require('mongoskin');
var StellarSdk = require('stellar-sdk');
var request = require('request');
var stellarNetwork = config.stellarNetwork;
var db = mongo.db(config.connectionString, {
    native_parser: true
});
db.bind('account');

if (stellarNetwork === "test") {
    StellarSdk.Network.useTestNetwork();
} else if (stellarNetwork === "public") {
    StellarSdk.Network.usePublicNetwork();
}
var server = new StellarSdk.Server(config.stellarServer);

var service = {};
service.signTxn = signTxn;
service.xdrToEnv = xdrToEnv;

module.exports = service;

/**
 * @author: Akshay Misal
 * @description: Set the transaction between two org.
 * @param {sourceAccount, sourceSeed, destination, asset, amount} req 
 * @param {transactionSummary} res 
 */
function signTxn(req, res) {
    var deferred = Q.defer();
    var sourceKeypair = StellarSdk.Keypair.fromSecret(req.body.sourceSeed);
    
    if(req.body.asset === 'Native'){
       var asset = StellarSdk.Asset.native()
    }else {
       var asset = StellarSdk.Asset.native()
    }
    
    server.loadAccount(req.body.sourceAccount)
        .then(function (account) {
            var transaction = new StellarSdk.TransactionBuilder(account)

                .addOperation(StellarSdk.Operation.payment({
                    destination: req.body.destination,
                    asset: asset,
                    amount: req.body.amount,
                }))
                .build();

            // console.log(transaction.toEnvelope().toXDR('base64'));
            transaction.sign(sourceKeypair);

            server.submitTransaction(transaction)
                .then(function (transactionResult) {
                    // console.log(JSON.stringify(transactionResult, null, 2));
                    // console.log('\nSuccess! View the transaction at: ');
                    // console.log(transactionResult._links.transaction.href);
                    deferred.resolve(transactionResult)
                })
                .catch(function (err) {
                    // console.log('An error has occured:');
                    // console.log(err);
                    deferred.reject(err)
                });

        })
        .catch(function (e) {
            // console.error(e);
            deferred.reject("failed to get account details")
        });
    return deferred.promise;
}

/**
 * @author:Akshay Misal
 * @description: Xdr to envelope
 * @param {xdr}  req
 * @param {envelope} res
 */
function xdrToEnv(req, res) {
    var deferred = Q.defer();

    var data = req.body.xdr;

    var envelope = StellarSdk.xdr.TransactionEnvelope.fromXDR(data, "base64");

    deferred.resolve(envelope);
    return deferred.promise;
}